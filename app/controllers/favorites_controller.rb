class FavoritesController < ApplicationController
  def show; end

  def destroy
    FavoriteUser.find(params[:id]).destroy!
    redirect_to favorites_path
  end

  def create
    @user = User.find(params[:id])
    @favorite_user = FavoriteUser.new
    @favorite_user.favorit = @user
    @favorite_user.favoriter = current_user
    @favorite_user.save
    redirect_to user_path
  end
end
