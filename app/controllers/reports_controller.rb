class ReportsController < ApplicationController
  def create
    @report = Report.create_with_sender_and_reported_user(report_params,
                                                          current_user,
                                                          User.find(params[:id]))
    redirect_to user_path(id: params[:id])
  end

  private

  def report_params
    params.require(:report).permit(:description)
  end
end
