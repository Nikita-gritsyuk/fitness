class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
    @report = Report.new
  end

  def ban
    raise 'operation not permited' unless can? :ban, User

    @user = User.find(params[:id])
    @user.role = :blocked
    @user.save
    render 'users/show'
  end

  def activate
    raise 'operation not permited' unless can? :activate, User
    @user = User.find(params[:id])
    @user.role = :user
    @user.save
    render "users/show"
  end

  def verify_user
    raise 'operation not permited' unless can? :verify, User
    @user = User.find(params[:id])
    @user.update(verified: true)
    redirect_to user_url(@user)
  end

  def edit
  end

  def self_destroy
    current_user.destroy
    flash[:success] = "Your account deleted"
    redirect_to users_url
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def update
    if current_user.update(user_params)
      flash[:success] = "Team Member successfully Updated!"
    else
      flash[:danger] = "Team Member unsuccessfully Updated!"
    end
    render 'edit'
  end

  def index
    @filterrific = initialize_filterrific(
      User,
      params[:filterrific],
      select_options: {
        sorted_by: User.options_for_sorted_by
      },
      persistence_id: "shared_key",
      sanitize_params: true,
    ) || return

    @users = @filterrific.find.page(params[:page])

    # Respond to html for initial page load and to js for AJAX filter updates.
    respond_to do |format|
      format.html
      format.js {render partial: 'users/index.js.erb'}
    end

  rescue ActiveRecord::RecordNotFound => e
    # There is an issue with the persisted param_set. Reset it.
    puts "Had to reset filterrific params: #{e.message}"
    redirect_to(reset_filterrific_url(format: :html)) && return
  end

  private
  def user_params
    params.require(:user).permit(:first_name, :last_name, :avatar)
  end
end
