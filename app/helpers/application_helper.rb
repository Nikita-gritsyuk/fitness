module ApplicationHelper
  def user_avatar(user)
    if current_user.avatar.attached?
      current_user.avatar.variant(loader: { page: nil }, resize_to_limit: [150, nil])
    end
  end
end
