class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    can :read, :all
    case user.role
    when 'admin'
      can :verify, User
      can :ban, User
      can :activate, User
    end
  end
end
