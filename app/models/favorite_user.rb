class FavoriteUser < ApplicationRecord
  belongs_to :favorit, class_name: 'User'
  belongs_to :favoriter, class_name: 'User'

  validates_each :favorit_id, :favoriter_id do |record, attr, _value|
    record.errors.add(attr, 'The user has already been added as a friend') if record.favorit_id == record.favoriter_id
  end

  def self.create_with_favorit_and_favoriter
    favorite_user = FavoriteUser.new(params[:id])
    favorite_user.favorit = favorit
    favorite_user.favoriter = favoriter
    favorite_user.save
    favorite_user
  end
end
