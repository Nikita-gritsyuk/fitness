class Report < ApplicationRecord
  belongs_to :sender, class_name: 'User'
  belongs_to :reported_user, class_name: 'User'

  def self.create_with_sender_and_reported_user(params, sender, reported_user)
    report = Report.new(params)
    report.sender = sender
    report.reported_user = reported_user
    report.save
    report
  end
end
