class User < ApplicationRecord

  has_many :reports, foreign_key: :reported_user_id
  has_many :reports_sent, class_name: 'Report', foreign_key: :sender_id
  has_many :reported_users, through: :reports
  has_many :reported_by_users, through: :reports, source: :sender
  has_many :favorite_records, foreign_key: :favoriter_id, class_name: 'FavoriteUser'
  has_many :favorite_users, through: :favorite_records, source: :favorit
  has_one_attached :avatar

  acts_as_paranoid

  before_validation :set_random_password
  

  include Filterable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, omniauth_providers: [:google_oauth2]

  enum role: [:user, :admin, :blocked, :activate]

  def have_favorite?(user)
    FavoriteUser.where(favoriter_id: id, favorit_id: user.id).any?
  end

  def self.from_google(email:, uid:)
    create_with(uid: uid).find_or_create_by!(email: email)
  end

  def active_for_authentication?
    super && !blocked?
  end
  

  filterrific(
    default_filter_params: { sorted_by: "first_name_desc"},
    available_filters: [:sorted_by, :search_query, :with_created_at_gte]
  )

  scope :search_query, ->(query) {
    return nil  if query.blank?

    # condition query, parse into individual keywords
    terms = query.downcase.split(/\s+/)
  
    # replace "*" with "%" for wildcard searches,
    # append '%', remove duplicate '%'s
    terms = terms.map { |e|
      (e.tr("*", "%") + "%").gsub(/%+/, "%")
    }

    # configure number of OR conditions for provision
    # of interpolation arguments. Adjust this if you
    # change the number of OR conditions.
    num_or_conds = 2
    
    where(
      terms.map { |_term| "(LOWER(users.first_name) LIKE ? OR LOWER(users.last_name) LIKE ?)"}.join(" AND "),
      *terms.map { |e| [e] * num_or_conds }.flatten,
    )
  }

  scope :sorted_by, ->(sort_option) {
    direction = /desc$/.match?(sort_option) ? "desc" : "asc"
    case sort_option.to_s
    when /^first_name/
      order("LOWER(users.first_name) #{direction}")
    when /^email/
      order("LOWER(users.email) #{direction}")
    when /^created_at/
      order("LOWER(users.created_at) #{direction}")
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }
  scope :with_created_at_gte, ->(ref_date) {
    where("users.created_at >= ?", reference_time)
  }

  # This method provides select options for the `sorted_by` filter select input.
  # It is called in the controller as part of `initialize_filterrific`.
  def self.options_for_sorted_by
    [
      ["Name (a-z)", "first_name_asc"],
      ["Registration date (newest first)", "created_at_desc"],
      ["Registration date (oldest first)", "created_at_asc"],
    ]
  end

  private

  def set_random_password
    if self.uid.present? && self.password.nil?
      self.password = SecureRandom.urlsafe_base64
      self.password_confirmation = self.password
    end
  end
end
