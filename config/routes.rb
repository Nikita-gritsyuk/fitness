Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  root 'welcome#index'

  get '/welcome', to: 'welcome#index', as: 'welcome_index'
  get '/favorites', to: 'favorites#show', as: 'favorites'

  delete '/favorites/:id/delete_from_favorites', to: 'favorites#destroy', as: 'delete_from_favorites'

  post '/users/:id/add_to_favorites', to: 'favorites#create', as: 'add_to_favorites'
  post '/users/:id/activate_user', to: 'users#activate', as: 'activate_user'
  post '/users/:id/ban_user', to: 'users#ban', as: 'ban_user'
  post '/users/:id/report', to: 'reports#create', as: 'report_user'
  post '/users/:id/verify_user', to: 'users#verify_user', as: 'verify_user'

  get '/users/edit_profile', to: 'users#edit', as: 'edit_profile'
  put '/users/edit_profile', to: 'users#update'
  get "/users", to: "users#index"

  get '/users/:id', to: 'users#show', as: 'user'
  delete '/users/:id', to: 'users#destroy', as: "user_delete"
  delete '/edit_profile/delete_profile', to: 'users#self_destroy', as: "user_self_destroy"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
