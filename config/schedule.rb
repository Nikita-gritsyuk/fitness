set :output, "#{Whenever.path}/log/cron.log"

every 1.day do
  rake "task:clearing_up", :environment => :development
end