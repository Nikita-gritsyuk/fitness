class CreateReports < ActiveRecord::Migration[6.1]
  def change
    create_table :reports do |t|
      t.belongs_to :sender, class_name: 'User'
      t.belongs_to :reported_user, class_name: 'User'
      t.text :description

      t.timestamps
    end
  end
end
