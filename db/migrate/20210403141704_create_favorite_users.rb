class CreateFavoriteUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :favorite_users do |t|
      t.belongs_to :favorit, class_name: 'User'
      t.belongs_to :favoriter, class_name: 'User'
      
      t.timestamps
    end
  end
end
