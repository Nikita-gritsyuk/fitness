class AddClearedUpToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :cleared_up, :boolean, default: false
  end
end
