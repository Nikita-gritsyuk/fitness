class AddUniqIndexToFavoriteUser < ActiveRecord::Migration[6.1]
  def change
    add_index :favorite_users, [:favorit_id, :favoriter_id], unique: true
  end
end
