require 'securerandom'

namespace :task do
  desc "clearing up deleted users"
  task clearing_up: :environment do
    @users = User.with_deleted.where({ deleted_at: (2000.years.ago)..(Time.now - 30.days), cleared_up: false})
    puts "Clearing up #{@users.count}"
    @users.each do |user|
      puts "Clearing up ##{user.id}"
      user.update( first_name: 'removed user',
                   last_name: "", 
                   email: "#{SecureRandom.alphanumeric(8)}@replaced.replaced", 
                   avatar: user.avatar.destroy!, 
                   cleared_up: true)
    end
  end
end
